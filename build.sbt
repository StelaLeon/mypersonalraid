name := "FileSystemPureFP"

version := "0.1"

scalaVersion := "2.13.3"
val catsVersion = "2.1.1"

//scalacOptions += "-Ypartial-unification" //no need anymore ... if scala <2.13 then please enable this here and in the plugins


libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsVersion,
  "org.typelevel" %% "cats-macros" % catsVersion,
  "org.typelevel" %% "cats-kernel" % catsVersion,
  "org.typelevel" %% "simulacrum" % "1.0.0"
)
