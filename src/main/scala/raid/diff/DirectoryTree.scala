package raid.diff

import cats.Applicative

object directorytree {
  sealed abstract class DirectoryTree[A] extends Product with Serializable {
    def traverse[F[_]: Applicative, B](f: A => F[B]): F[DirectoryTree[B]] = this match {
      case DirectoryTree.Empty()         => Applicative[F].pure(DirectoryTree.Empty())
      case DirectoryTree.Branch(files, subdir ) => Applicative[F].map2(f(files), traverseSubDirs(subdir)(f))(DirectoryTree.Branch(_, _))
    }

    def traverseSubDirs[F[_]: Applicative,B](subdirs: List[DirectoryTree[A]]) (f:A=>F[B]): F[List[DirectoryTree[B]]] = {
      subdirs.foldRight(Applicative[F].pure(List.empty[DirectoryTree[B]])){(a,acc) =>
        Applicative[F].map2(a.traverse(f),acc)(_ :: _)
      }
    }
  }
  object DirectoryTree {
    final case class Empty[A]() extends DirectoryTree[A]
    final case class Branch[A](files: A, subdirectories: List[DirectoryTree[A]]) extends DirectoryTree[A]
  }
}
