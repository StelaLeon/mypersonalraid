package raid.diff

import scala.concurrent.{ExecutionContext, Future}

case class RaidFile(name: String, path: String)

class DirectoryOps {

/*  // Example implementation for right-biased Either
  implicit def applicativeForFirectory[L]: Applicative[DirectoryTree[List[raid.diff.RaidFile]]] = new Applicative[DirectoryTree[List[raid.diff.RaidFile]]] {
    def product[A](fa: DirectoryTree[A], fb: DirectoryTree[A]): DirectoryTree[A] = (fa, fb) match {
      case (DirectoryTree.Empty(), _) => _
      case (_ , DirectoryTree.Empty()) => _
      case (Branch(a1,subtree1), Branch(a2, subtree2)) => DirectoryTree.Branch(a1+a2, product[A](subtree1, subtree2))
    }

    def pure[A](a: A, subdirs: List[DirectoryTree[A]]): DirectoryTree[A]= DirectoryTree.Branch(a,subdirs)

    def map[A, B](fa: DirectoryTree[A])(f: A => B): DirectoryTree[B] = fa match {

    }
  }*/

  def writeFilesOnDisk(as: List[RaidFile])(f: RaidFile => Future[Int])(implicit ec: ExecutionContext): Future[List[Int]] = {
    //int is the number of bytes or size of the file that has been written
    Future.traverse(as)(f)
  }


}
